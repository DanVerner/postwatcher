package com.saveliev.postswatcher

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.item_detail.*
import kotlinx.android.synthetic.main.item_detail.view.*
import kotlinx.android.synthetic.main.item_list_comment.*
import java.io.InputStreamReader
import java.net.URL

class ItemDetailFragment : Fragment() {

    private var mHandler: Handler? = null
    private var item: Post? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createHandler()

        arguments?.let {
            if (it.containsKey("post_item")) {
                item = it.getParcelable("post_item") as Post
                activity?.toolbar_layout?.title = item?.title
            }
        }

        downloadUserData(item?.userId)
        downloadCommentData(item?.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.item_detail, container, false)

        item?.let {
            rootView.item_detail.text = it.body
        }

        return rootView
    }

    private fun setAuthor(name: String){
        item_author.text = name
    }

    private fun setComments(listComments: ArrayList<Comment>){
        val progress = progress_comments
        val recyclerView = item_list_comment

        recyclerView.addItemDecoration(DividerItemDecoration(activity?.applicationContext, 1))
        recyclerView.adapter = CommentRecyclerViewAdapter(listComments)
        progress.visibility = View.GONE
    }

    private fun downloadUserData(userId: String?) {
        val thread = Thread(Runnable {
            val url = URL(Constants.URL + "users?id=" + userId)
            val reader = InputStreamReader(url.openStream())

            val jsonArray = Gson().fromJson(reader, JsonArray::class.java)
            val jsonObject = jsonArray[0].asJsonObject
            val authorName = jsonObject.get("name").asString

            val msg = mHandler?.obtainMessage(Constants.LOADED_USER_DATA, authorName)
            mHandler?.sendMessage(msg)
       })
        thread.start()
    }

    private fun downloadCommentData(postId: String?) {
        val thread = Thread(Runnable {
            val url = URL(Constants.URL + "comments?postId=" + postId)
            val reader = InputStreamReader(url.openStream())
            val listType = object : TypeToken<ArrayList<Comment>>() {}.type
            val listComments = Gson().fromJson<ArrayList<Comment>>(reader, listType)

            val msg = mHandler?.obtainMessage(Constants.LOADED_COMMENT_DATA, listComments)
            mHandler?.sendMessage(msg)
        })
        thread.start()
    }

    private fun createHandler() {
        mHandler = Handler { message ->
            when (message.what) {
                Constants.LOADED_USER_DATA -> setAuthor(message.obj as String)
                Constants.LOADED_COMMENT_DATA -> setComments(message.obj as ArrayList<Comment>)
            }
            true
        }
    }
}
