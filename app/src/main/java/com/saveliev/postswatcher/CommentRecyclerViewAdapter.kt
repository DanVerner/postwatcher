package com.saveliev.postswatcher

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentRecyclerViewAdapter(
    private val listComments: ArrayList<Comment>
) : RecyclerView.Adapter<CommentRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.text_name
        val email: TextView = view.text_email
        val comment: TextView = view.text_comment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount() = listComments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val comment = listComments[position]

        holder.title.text = comment.name
        holder.email.text = comment.email
        holder.comment.text = comment.body
    }
}