package com.saveliev.postswatcher

object Constants {
    const val LOADED_POSTS_DATA = 1
    const val LOADED_USER_DATA = 2
    const val LOADED_COMMENT_DATA = 3

    const val URL = "https://jsonplaceholder.typicode.com/"
}