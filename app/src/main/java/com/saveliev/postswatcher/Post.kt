package com.saveliev.postswatcher

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    val userId: String,
    val id: String,
    val title: String,
    val body: String
) : Parcelable {
    override fun toString(): String = title
}