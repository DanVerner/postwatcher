package com.saveliev.postswatcher

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.provider.Settings.Global.getString
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list_content.view.*
import kotlinx.android.synthetic.main.item_list.*
import java.io.InputStreamReader
import java.net.URL

class ItemListActivity : AppCompatActivity() {

    private var mHandler: Handler? = null
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        createHandler()

        if (item_detail_container != null) {
            twoPane = true
        }

        downloadPostsData()
    }

    private fun setupRecyclerView(posts: ArrayList<Post>) {
        val recyclerView = item_list

        recyclerView.addItemDecoration(DividerItemDecoration(this, 1))
        recyclerView.adapter = SimpleItemRecyclerViewAdapter(this, posts, twoPane)
    }

    private fun downloadPostsData() {
        val thread = Thread(Runnable {
            val url = URL(Constants.URL + "posts")
            val reader = InputStreamReader(url.openStream())
            val listType = object : TypeToken<ArrayList<Post>>() {}.type
            val listPosts = Gson().fromJson<ArrayList<Post>>(reader, listType)

            val msg = mHandler?.obtainMessage(Constants.LOADED_POSTS_DATA, listPosts)
            mHandler?.sendMessage(msg)
        })
        thread.start()
    }

    private fun createHandler() {
        mHandler = Handler { message ->
            when(message.what) {
                Constants.LOADED_POSTS_DATA -> setupRecyclerView(message.obj as ArrayList<Post>)
            }
            true
        }
    }

    class SimpleItemRecyclerViewAdapter(
        private val parentActivity: ItemListActivity,
        private val posts: ArrayList<Post>,
        private val twoPane: Boolean
    ) :
        RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener

        init {
            onClickListener = View.OnClickListener { v ->
                val item = v.tag as Post
                if (twoPane) {
                    val fragment = ItemDetailFragment().apply {
                        arguments = Bundle().apply {
                            putParcelable("post_item", item)
                        }
                    }
                    parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit()
                } else {
                    val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                        putExtra("post_item", item)
                    }
                    v.context.startActivity(intent)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = posts[position]
            holder.idView.text = parentActivity.getString(R.string.label_number,item.id)
            holder.contentView.text = item.title

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
            }
        }

        override fun getItemCount() = posts.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val idView: TextView = view.id_text
            val contentView: TextView = view.content
        }
    }
}
